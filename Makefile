include theos/makefiles/common.mk

TOOL_NAME = exifdate
exifdate_FILES = main.c
exifdate_FRAMEWORKS = CoreFoundation ImageIO

include $(THEOS_MAKE_PATH)/tool.mk
