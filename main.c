#include <ImageIO/ImageIO.h>

static void $_saveTimestamp(CFURLRef URL,CFDateFormatterRef formatter) {
  CGImageSourceRef source=CGImageSourceCreateWithURL(URL,NULL);
  if(!source){return;}
  CFDictionaryRef metadata=CGImageSourceCopyPropertiesAtIndex(source,0,NULL);
  CFRelease(source);
  if(!metadata){return;}
  CFDictionaryRef exif=CFDictionaryGetValue(metadata,kCGImagePropertyExifDictionary);
  if(exif){
    CFStringRef datestr=CFDictionaryGetValue(exif,CFSTR("DateTimeOriginal"));
    if(datestr){
      CFDateRef date=CFDateFormatterCreateDateFromString(NULL,formatter,datestr,NULL);
      if(date){
        CFShow(URL);
        CFShow(datestr);
        CFDictionaryRef attr=CFDictionaryCreate(NULL,
         (const void*[]){kCFURLCreationDateKey,kCFURLContentModificationDateKey},
         (const void*[]){date,date},2,NULL,NULL);
        CFErrorRef error;
        if(!CFURLSetResourcePropertiesForKeys(URL,attr,&error)){CFShow(error);}
        CFRelease(attr);
        CFRelease(date);
      }
    }
  }
  CFRelease(metadata);
}

int main(int argc,char** argv) {
  if(argc<2){
    printf("Usage: %s [path...]\n",argv[0]);
    return 1;
  }
  CFDateFormatterRef formatter=CFDateFormatterCreate(NULL,NULL,
   kCFDateFormatterNoStyle,kCFDateFormatterNoStyle);
  CFDateFormatterSetFormat(formatter,CFSTR("yyyy:MM:dd HH:mm:ss"));
  int i;
  for (i=1;i<argc;i++){
    CFURLRef URL=CFURLCreateFromFileSystemRepresentation(NULL,
     (const UInt8*)argv[i],strlen(argv[i]),false);
    $_saveTimestamp(URL,formatter);
    CFURLEnumeratorRef enumerator=CFURLEnumeratorCreateForDirectoryURL(NULL,
     URL,kCFURLEnumeratorDescendRecursively,NULL);
    CFRelease(URL);
    while(CFURLEnumeratorGetNextURL(enumerator,&URL,
     NULL)==kCFURLEnumeratorSuccess){$_saveTimestamp(URL,formatter);}
    CFRelease(enumerator);
  }
  CFRelease(formatter);
}
